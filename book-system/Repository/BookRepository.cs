﻿using System;
using book_system.Models;

namespace book_system.Repository
{
    public class BookRepository
    {
        public Book [] books = {
            new Book(0, "SOA Architecture", "English" ),
            new Book(1, "Introduction To Kubernetes", "English" ),
            new Book(2, "Deep Learning", "English" ),
        };  
    }
}
