﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using book_system.Models;
using book_system.Repository;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace book_system.Controllers
{
    [Route("api/[controller]")]
    public class BookController : Controller
    {
        public static BookRepository bookRepository = new BookRepository();
        public Book[] books = bookRepository.books;

        // GET: api/values
        [HttpGet]
        public IEnumerable<Book> Get()
        {
            return books;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Book Get(int id)
        {
            return books[id];
        }

        // POST api/values
        [HttpPost]
        public IEnumerable<Book> Post([FromBody] Book book)
        {
            books[book.id] = new Book(book.id, book.title, book.language);
            return books;
        }

        // PUT api/values/5
        [HttpPut]
        public IEnumerable<Book> Put([FromBody] Book book)
        {
            books[book.id] = new Book(book.id, book.title, book.language);
            return books;

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
