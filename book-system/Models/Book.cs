﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace book_system.Models
{
    [Table("Books")]
    public class Book
    {
        [Key]
        [Required]
        public int id { set; get; }

        [Required]
        public string title { set; get; }

        [Required]
        public string language { set; get; }

        public Book()
        {
        }

        public Book(int id, string title, string language)
        {
            this.id = id;
            this.title = title;
            this.language = language;
        }
    }
}
